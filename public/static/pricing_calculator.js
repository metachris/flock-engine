$ = function(e) { return document.getElementById(e); }

function addCommas(nStr) {
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}


function getCosts(hrs) {        
    // $ per 1.000 connection-hours
    pricing = new Array(1.00, 0.9, 0.80);
    
    package_costs = new Array(0, 0, 0);
    package_steps = 100000;

    credits = 0;
    
    package = 0;
    while (hrs > package_steps) {
        price_per_k = pricing[package];
            
        credits += parseInt((package_steps/1000) * price_per_k);
        hrs -= package_steps;
        
        package_costs[package] += parseInt((package_steps/1000) * price_per_k);
        if (package < pricing.length - 1) {
            package++;
        }            
    }

    price_per_k = pricing[package];
    if (hrs > 0) {    
        credits += parseInt((hrs/1000) * price_per_k);
        package_costs[package] += parseInt((hrs/1000) * price_per_k);
    }
    
    // Display
    for (i=0; i<pricing.length; i++) {
        c = package_costs[i];
        if (c == 0) {
            $("x" + (i+1)).innerHTML = "";
            continue;
        } 
        if (c % 1 > 0) {
            if (!(c % 1 > 10))        
                c += "0";
        } else
            c += ".00";    
        $("x" + (i+1)).innerHTML = "$ " + c;
    }
    
    return credits    
}



function calc3() {
    players_per_day = parseFloat($("i31").value);
    time_per_player = parseFloat($("i32").value);

    game_seconds_per_day = players_per_day * time_per_player;
    game_hours_per_day = game_seconds_per_day / 60;
    
    cost_per_day = (game_hours_per_day / 1000) * 2;

    $("rf1").innerHTML = game_hours_per_day.toFixed(1) + " h";
    $("rf2").innerHTML = "$ " + addCommas(cost_per_day.toFixed(2));

    $("rf22").innerHTML = (cost_per_day > 5) ? "<a href='mailto:support@flockengine.com?subject=Pricing for high traffic games (" + game_hours_per_day.toFixed(0) + "h)'>contact us for a special offer</a>" : "";


    var url = '/static/calc.php?u=' + players_per_day + "&g=" + time_per_player + "&m=" + game_hours_per_day.toFixed(0) + "&c=" + cost_per_day.toFixed(2);
    new Ajax.Request(url, {
      method: 'get',
      onSuccess: function(transport) {}
    });    

/*
    avg_players_online = game_hours_per_day / 24;
    $("rs5").innerHTML = addCommas(avg_players_online.toFixed(0));
*/

/*

    conn_per_day = conn_per_week / 7;

    conn_per_week = active_users * avg_games_per_week;
    conn_per_hour = conn_per_day / 24.0;
    conn_per_min = parseFloat(conn_per_hour) / 60.0;
    avg_concurrent_games = avg_min_per_game * conn_per_min;
    
    connection_hours_per_month = (conn_per_day * avg_min_per_game * 30) / 60; 
    
    $("rs1").innerHTML = addCommas(parseInt(conn_per_week));
    $("rs2").innerHTML = addCommas(parseInt(conn_per_day));
    $("rs3").innerHTML = addCommas(parseInt(conn_per_hour));
    $("rs4").innerHTML = addCommas(conn_per_min.toFixed(1));
    $("rs5").innerHTML = addCommas(parseInt(avg_concurrent_games));
    $("rs6").innerHTML = addCommas(parseInt(connection_hours_per_month));

    // Fix too low numbers for the price
    if (parseInt(avg_concurrent_games) == 0) {
        connection_hours_per_month = 0;
    }

    cost = connection_hours_per_month / 500;
    $("rs7").innerHTML = "$ " + cost.toFixed(2);

    // Save query

*/
}

